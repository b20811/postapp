package com.b208.postApp.services;

import com.b208.postApp.models.User;

import java.util.Optional;

public interface UserService {

    Iterable<User> getUsers();

    void createUser(User user);

    void updateUser(Long id, User user);

    void deleteUser(Long id);

    Optional<User> findByUsername(String username);
}
