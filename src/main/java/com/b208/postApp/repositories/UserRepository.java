package com.b208.postApp.repositories;

import com.b208.postApp.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User,Object> {
    //Custom Method - findBy* is an automatic and a convention for creating a customized query method via our CrudRepository
    User findByUsername(String username);
}
